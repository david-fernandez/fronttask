import React,{ useState, useEffect, createElement } from 'react';
import { API } from '../../shared/services/api';


function GetApi (){

    const [list, setList] = useState([]);

    useEffect(() => {
        API.get().then(res => {
            console.log(res.data);
            setList(res.data);
        });

    }, [])

    const orderList = list.sort(function (a, b) {
        if (a.code > b.code) {
            return 1;
          }
          if (a.code < b.code) {
            return -1;
          }
        }
    );

    function dividirCodigo (code){
        let array = [];
        let stringCode = "";
        let j = 0;
        let k = 1;
        stringCode = String(code).split('', 8);
        for (let i=0; i<4; i++) {
            array[i] = stringCode[j] + stringCode[k];
            j += 2;
            k += 2;
        }
        return array;
    }

    function crearTemplate (list){
        let template = [];
        let arrayCode1 = [];
        let arrayCode2 = [];

        //let ul = document.createElement("ul");

        for (let i=1; i<list.length; i++){
            arrayCode1 = dividirCodigo(list[i-1].code);
            arrayCode2 = dividirCodigo(list[i].code);
            if (i === 1){
                template.push(
                    <li>{list[i-1].code} {list[i-1].name}</li>
                );
                // var li = document.createElement("li");
                // let text = document.createTextNode(list[i-1].code + " " + list[i-1].name);
                // ul.appendChild(li);
                // li.appendChild(text);
                //console.log(ul);
                //template.push(ul);
            }
            if (arrayCode1[0] === arrayCode2[0]){
                // Misma Seccion
                if (arrayCode1[1] === arrayCode2[1]){
                    // Misma Familia
                    if (arrayCode1[2] === arrayCode2[2]){
                        // Misma Clase
                        if (arrayCode1[3] !== arrayCode2[3]){
                            template.push(
                                <ul>
                                    <ul>
                                        <ul>
                                            <li>{list[i].code} {list[i].name}</li>
                                        </ul>
                                    </ul>
                                </ul>
                            );
                            // var li4 = document.createElement("li");
                            // let text = document.createTextNode(list[i].code + " " + list[i].name);
                            // li4.appendChild(text);
                            // li3.appendChild(text);
                            // li2.appendChild(li3);
                            // li.appendChild(li2);
                            // ul.appendChild(li);
                        }
                    }else{
                        // Distinta Clase
                        template.push(
                            <ul>
                                <ul>
                                    <li>{list[i].code} {list[i].name}</li>
                                </ul>
                            </ul>
                        );
                        // var li3 = document.createElement("li");
                        // let text = document.createTextNode(list[i].code + " " + list[i].name);
                        // li3.appendChild(text);
                        // li2.appendChild(li3);
                        // li.appendChild(li2);
                        // ul.appendChild(li);
                    }
                }else{
                    // Distinta Familia
                    template.push(
                        <ul>
                            <li>{list[i].code} {list[i].name}</li>
                        </ul>
                    );
                    // var li2 = document.createElement("li");
                    // let ul2 = document.createElement("ul");
                    // let text = document.createTextNode(list[i].code + " " + list[i].name);
                    // li2.appendChild(text);
                    // li.appendChild(li2);
                    // ul.appendChild(li);
                }
            }else{
                // Distinta Seccion
                template.push(
                    <li>{list[i].code} {list[i].name}</li>
                );
                // var li = document.createElement("li");
                // let text = document.createTextNode(list[i].code + " " + list[i].name);
                // li.appendChild(text);
                // ul.appendChild(li);
                // console.log(ul);
            }
        }
        //console.log(ul);
        return template;
    }

    let ul = crearTemplate(orderList);
    //console.log(document.getElementById("content-list").value);

    return (
        <div>
            <div>
                {ul && <ul>
                    {ul}
                </ul>}
            </div>
        </div>
    );
}

export default GetApi;
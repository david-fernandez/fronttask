import React, { createElement, useState } from 'react';
import GetApi from '../GetApi/GetApi';
import PostApi from '../PostApi/PostApi';
import './HomePage.scss';


function HomePage() {

    const [viewGetApi, setViewGetApi] = useState(false);
    const [viewPostApi, setViewPostApi] = useState(false);

    function fnViewGetApi (){
        if (viewGetApi){
            setViewGetApi(false);
        }else{
            setViewGetApi(true);
        }
    }

    function fnViewPostApi (){
        if (viewPostApi){
            setViewPostApi(false);
        }else{
            setViewPostApi(true);
        }
    }

    return (
        <div className="content-flex">
                <div className="content-btn">
                    <button className="btn" onClick={fnViewGetApi}>List of Products</button>
                    {viewGetApi && <GetApi/>}
                </div>
                <div className="content-btn">
                    <button className="btn" onClick={fnViewPostApi}>Add new Product</button>
                    {viewPostApi && <PostApi/>}
                </div>

                {/* <div>
                    <ul>
                        <li>
                            Seccion 1
                            <ul>
                                <li>Parte 1</li>
                                <li>Parte 2</li>
                            </ul>
                        </li>
                        <li>
                            Seccion 2
                            <ul>
                                <li>Parte 1</li>
                                <li>Parte 2</li>
                            </ul>
                        </li>
                    </ul>
                </div> */}



        </div>
    );
}

export default HomePage;
import React from 'react';
import {useForm} from 'react-hook-form';
import { API } from '../../shared/services/api';
import './PostApi.scss';

function PostApi (){

    const {register, handleSubmit} = useForm();

    function onSubmit(formData){
        
        if (validation(formData)){
            API.post(formData).then( res =>{
                console.log(formData);
            });
        }
    }

    function validation (data){
        let arrayDividido = [];
        let valido = false;
        arrayDividido = dividirCodigo(data.code);

        if (arrayDividido[0] < 10){
            console.log("Segmento no valido");
        }else{
            if (arrayDividido[0] >= "10" && arrayDividido[0] <= "15"){
                console.log("Segmento 10 - 15");
                valido = true;
            }else{
                if (arrayDividido[0] >= "20" && arrayDividido[0] <= "27"){
                    console.log("Segmento 20 - 27");
                    valido = true;
                }else{
                    if (arrayDividido[0] >= "30" && arrayDividido[0] <= "41"){
                        console.log("Segmento 30 - 41");
                        valido = true;
                    }else{
                        if (arrayDividido[0] >= "42" && arrayDividido[0] <= "60"){
                            console.log("Segmento 42 - 60");
                            valido = true;
                        }else{
                            if (arrayDividido[0] >= "70" && arrayDividido[0] <= "94"){
                                console.log("Segmento 70 - 94");
                                valido = true;
                            }else{
                                if (arrayDividido[0] == "95"){
                                    console.log("Segmento 95");
                                    valido = true;
                                }else{
                                    console.log("Segmento no válido");
                                }
                            }
                        }
                    }
                }
            }
        }
        return valido;
    }

    function dividirCodigo (code){
        let array = [];
        let stringCode = "";
        let j = 0;
        let k = 1;
        stringCode = String(code).split('', 8);
        for (let i=0; i<4; i++) {
            array[i] = stringCode[j] + stringCode[k];
            j += 2;
            k += 2;
        }
        return array;
    }

    return (
        <div>
            <div className="content-form">
                <form onSubmit={handleSubmit(onSubmit)}>
                    <label htmlFor="code">Code:</label>
                    <input id="code" type="number" name="code" ref={register({required: true, minLength: 8, maxLength: 8})}/>
                    <label htmlFor="name">Name:</label>
                    <input id="name" type="text" name="name" ref={register({required: true})}/>
                    <button>Add</button>
                </form>
            </div>
        </div>
    );
}

export default PostApi;
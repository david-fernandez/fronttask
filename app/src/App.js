import './App.scss';
import HomePage from './pages/HomePage/HomePage';

function App() {
  return (
    <div className="App">
      <div className="App-header">
        <HomePage/>
      </div>
    </div>
  );
}

export default App;
